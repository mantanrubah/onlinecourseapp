const axios = require('axios');

const TIMEOUT = process.env.TIMEOUT || 5000;
const API_URL = process.env.API_URL || 'http://localhost:3000';

module.exports = (basUrl) => {
    return axios.create({
        baseURL: API_URL,
        timeout: TIMEOUT,
    })
}